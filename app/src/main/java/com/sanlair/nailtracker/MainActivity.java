package com.sanlair.nailtracker;
//TODO: Length of long events = 3h
import android.Manifest;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.content.Context;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.content.res.Resources.Theme;

import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.EntityReference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    static int activeTab = 0;
    public static int bcounter = 0;
    public StorageHelper dbhelper = null;
    public static MainActivity NailTracker = null;
    public Schedule edThisSched = null;
    private static String phoneToCall = "";
    public FabHandler fabHandler = null;
    static List<WorkType> workTypes = new ArrayList<>();
    public SimpleDateFormat sdym = new SimpleDateFormat("yyyy-MM", Locale.getDefault());
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    //public static SimpleDateFormat sds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public SimpleDateFormat stf = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static SimpleDateFormat sdd = new SimpleDateFormat("yyyy-MM-dd(E)", Locale.getDefault());

    public static int[] ax = {0,0,0};

    @Override
    protected void onDestroy() {
        dbhelper.close();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        bcounter+=1;
        NailTracker.edThisSched = null;
        if(activeTab!=1)
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(1))
                .commit();
        else {
            if (bcounter % 3 == 0) {
                super.onBackPressed();
            }
            Toast.makeText(getApplicationContext(), "Для выхода нажмите 'назад' два раза", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbhelper = new StorageHelper(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        // Setup spinner
        Spinner spinner = findViewById(R.id.spinner);
        spinner.setAdapter(new MyAdapter(toolbar.getContext(), getResources().getStringArray(R.array.toolbar_items)));
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // When the given dropdown item is selected, show its contents in the
                // container view.
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                        .commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        fabHandler = new FabHandler(findViewById(R.id.fab));//fab = findViewById(R.id.fab);
        fabHandler.fab.setOnClickListener(view -> {
            switch (activeTab) {
                case 1:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, PlaceholderFragment.newInstance(10))
                            .commit();
                    break;
                default:
                    break;
            }
        });
        workTypes = dbhelper.getWorkTypes();
        NailTracker = this;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }
        if(id == R.id.action_workTypes){
            startActivity(new Intent(MainActivity.this, WorkTypesActivity.class));
            return true;
        }
        if (id == R.id.action_about){
            final AlertDialog alt_bld = new AlertDialog.Builder(MainActivity.this).create();
            TextView msg = new TextView(this);
            msg.setText(Html.fromHtml(" <h3>Nailtracker</h3><p>Приложение для ведения записей на маникюр и т.п.</p>" +
                    "        <p>Специально для <a href='https://www.instagram.com/wow_gel_lac_nogti_ulia/'>Юльки.</a></p>" +
                    "        <p>Исходники доступны <a href='https://bitbucket.org/fedaykin/nailtracker'>тут</a></p>" +
                    "<p>* Отметить запись как успешно завершённую - свайп влево<br/>" +
                    "* Отменить запись - свайп вправо.</p>"));
            msg.setMovementMethod(LinkMovementMethod.getInstance());
            msg.setClickable(true);
            alt_bld.setCancelable(true);
            alt_bld.setView(msg);
//            alt_bld.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    alt_bld.dismiss();
//                }
//            });
            alt_bld.show();
        }
        return super.onOptionsItemSelected(item);
    }


    private static class MyAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        MyAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                // Inflate the drop down using the helper's LayoutInflater
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = view.findViewById(android.R.id.text1);
            textView.setText(getItem(position));

            return view;
        }

        @Override
        public Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }

        @Override
        public void setDropDownViewTheme(Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public StorageHelper dbhelper = null;
        public Button btnSaveSchedule;
        private static final String ARG_SECTION_NUMBER = "section_number";
        public Context context = null;
        public View rootView = null;
        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {
            context = Objects.requireNonNull(getActivity()).getApplicationContext();
            if (dbhelper == null) dbhelper = new StorageHelper(context);
            activeTab = getArguments() != null ? getArguments().getInt(ARG_SECTION_NUMBER) : 0;
            //System.out.println("View switched:" + activeTab);

            btnSaveSchedule = null;
            rootView = null;
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
            if (NailTracker.fabHandler.fab.getVisibility() == View.INVISIBLE || NailTracker.fabHandler.fab.getVisibility() == View.GONE) NailTracker.fabHandler.fab.show();
            TextView textView;
            List<Schedule> schedules;
            String rdt = NailTracker.sdym.format(new Date(System.currentTimeMillis() + 86400 * 1000));
            String from = rdt+"-01 00:00:00";
            String till = rdt+"-31 00:00:00";
            switch (activeTab) {
                case 1:
                    textView = rootView.findViewById(R.id.section_label);
                    schedules = dbhelper.getFilteredSchedules(0,null,null);
                    if (schedules.size() == 0) {
                        textView.setText(getString(R.string.NoNearestSchedules));
                    } else {
                        textView.setText(getString(R.string.nearest));
                        //System.out.println("entries to display:" + schedules.size());
                        LinearLayout lContainer = rootView.findViewById(R.id.lContainer);
                        //RelativeLayout lContainer = rootView.findViewById(R.id.lContainer);
                        int ec=0;
                        for (Schedule entry : schedules) {
                            ec+=1;
                            View erow = inflater.inflate(R.layout.sched_list_item, lContainer, false);
                            erow.setContentDescription(String.valueOf(entry.getGid()));
                            RelativeLayout schedCardRL = erow.findViewById(R.id.schedCardRL);
                            TextView TVschedDate = erow.findViewById(R.id.entry_date);
                            TextView TVschedTime = erow.findViewById(R.id.entry_time);
                            TextView tvWType = erow.findViewById(R.id.TVworkType);
                            TextView tvName = erow.findViewById(R.id.entry_name);
                            RelativeLayout rlDate = erow.findViewById(R.id.rlDate);
                            TextView tvComment = erow.findViewById(R.id.tvComment);
                            Button btnEditSched = erow.findViewById(R.id.btnEditSched);
                            btnEditSched.setContentDescription(String.valueOf(entry.getGid()));
                            String[] tfdate = entry.getStart_time().split(" ");
                            try {
                                Date tdate = sdf.parse(tfdate[0]);
                                tfdate[0] = sdd.format(tdate);
                                tfdate[1] = tfdate[1].substring(0,tfdate[1].length()-3);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
//                            TVschedDate.setText(entry.getStart_time());

                            TVschedDate.setText(tfdate[0]);
                            if(ec==1){
                                schedCardRL.setBackground(getResources().getDrawable(R.drawable.background_card_soon));
                                rlDate.setBackground(getResources().getDrawable(R.drawable.background_red));
                                TVschedTime.setBackgroundColor(getResources().getColor(R.color.colorREEEED));
                            }

                            TVschedTime.setText(tfdate[1]);
                            tvWType.setText(dbhelper.getWorkType(entry.getWork_type()));
                            tvName.setText(entry.getClient());
                            tvComment.setText(entry.getComment());
                            tvComment.setOnClickListener(v -> {
                                TextView tv = (TextView)v;

                                String str = tv.getText().toString().replaceAll("\\D+","");
                                if(str.length()>=10&&str.length()<12){
                                    phoneToCall=str;
                                    makeCall();
                                }
                            });
                            //erow.
                            erow.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    if(ax[2]==0)ax[2]=(int)event.getX();
                                    if(ax[2]>(int)event.getX()){
                                        erow.setTranslationX(erow.getTranslationX()-10);
                                        ax[2]=(int)event.getX();
                                    }else{
                                        erow.setTranslationX(erow.getTranslationX()+10);
                                        ax[2]=(int)event.getX();
                                    }
                                    if(event.getAction()==MotionEvent.ACTION_CANCEL || event.getAction()==MotionEvent.ACTION_UP){
                                        ax[1] = (int)event.getX();
                                        if(ax[0]-ax[1]>=150){
                                            erow.setTranslationX(0);
                                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                            builder.setTitle("Получено");
                                            final View lv = v;
                                            final EditText input = new EditText(getContext());
                                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                                            builder.setView(input);
                                            v.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                                            builder.setPositiveButton("OK", (dialog, which) -> {
                                                if(input.getText().toString().length()>0)
                                                dbhelper.changeStatus(lv.getContentDescription().toString(),1,Integer.parseInt(input.getText().toString()));
                                                v.setVisibility(View.GONE);
                                            });
                                            builder.setNegativeButton("Отмена", (dialog, which) -> dialog.cancel());
                                            builder.show();
                                            //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
                                            //right to left - ok
                                        }else if(ax[0]-ax[1]<=-100){
                                            erow.setTranslationX(0);
                                            //left to right -- cancel
                                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                            v.setBackgroundColor(getResources().getColor(R.color.colorRed));
                                            builder.setTitle("Отменить запись?");
                                            builder.setPositiveButton("OK", (dialog, which) -> {
                                                    dbhelper.changeStatus(v.getContentDescription().toString(),3);
                                                    v.setVisibility(View.GONE);
                                            });
                                            builder.setNegativeButton("Отмена", (dialog, which) -> dialog.cancel());
                                            builder.show();
                                            //v.setVisibility(View.GONE);
                                        }
                                        erow.setTranslationX(0);
                                    }
                                    else if(event.getAction()==MotionEvent.ACTION_DOWN){
                                        ax[0] = (int)event.getX();
                                    }
                                    return true;
                                }
                            });

                            btnEditSched.setOnClickListener(v -> {
                                bcounter=0;
                                NailTracker.edThisSched = dbhelper.getSchedule(Integer.parseInt(v.getContentDescription().toString()));
                                switch (activeTab) {
                                    case 1:
                                        NailTracker.getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.container, PlaceholderFragment.newInstance(10))
                                                .commit();
                                        break;
                                    default:
                                        break;
                                }
                            });
                            lContainer.addView(erow);
                        }
                    }
                    break;
                case 2:
                    bcounter=0;
                    NailTracker.fabHandler.fab.hide();
                    textView = rootView.findViewById(R.id.section_label);
                    //date filter and show all entries in selected date range
                    schedules = dbhelper.getFilteredSchedules(-1,from,till);
                    final LinearLayout lContainer = rootView.findViewById(R.id.lContainer);
                    final View filterRow = inflater.inflate(R.layout.list_filter, lContainer, false);
                    final TextView flStart = filterRow.findViewById(R.id.flStart);
                    final TextView flEnd = filterRow.findViewById(R.id.flEnd);
                    Button btnFilterOk = filterRow.findViewById(R.id.btnFilterOk);
                    btnFilterOk.setOnClickListener(v -> {
                        String dfrom = flStart.getText().toString()+" 00:00:00";
                        String dtill = flEnd.getText().toString()+ " 23:59:59";
                        List<Schedule> schedlist = dbhelper.getFilteredSchedules(-1,dfrom,dtill);
                        showFiltereSchedules(schedlist,inflater,lContainer);
                    });
                    //String[] tdate = from.split(" ");
                    flStart.setOnClickListener(v -> showDateDialog(v,flStart,Calendar.getInstance(),btnFilterOk));
                    flEnd.setOnClickListener(v -> showDateDialog(v,flEnd,Calendar.getInstance(),btnFilterOk));
                    flStart.setText(from.split(" ")[0]);
                    flEnd.setText(till.split(" ")[0]);
                    lContainer.addView(filterRow);
                    if (schedules.size() == 0) {
                        textView.setText(getString(R.string.NoEntries));
                    }else{
                       showFiltereSchedules(schedules,inflater,lContainer);
                    }
                    break;
                case 3:
                    bcounter=0;
                    //show filter row and stats for selected data range
                    //textView = rootView.findViewById(R.id.section_label);
                    //int[] workdone = dbhelper.getFinishedSchedules();
                    NailTracker.fabHandler.fab.hide();
                    final LinearLayout lContainerStats = rootView.findViewById(R.id.lContainer);
                    View filterRowStats = inflater.inflate(R.layout.list_filter, lContainerStats, false);
                    final TextView flStartStats = filterRowStats.findViewById(R.id.flStart);
                    final TextView flEndStats = filterRowStats.findViewById(R.id.flEnd);
                    Button btnFlOk = filterRowStats.findViewById(R.id.btnFilterOk);
                    btnFlOk.setOnClickListener(v -> {
                        String dfrom = flStartStats.getText().toString()+" 00:00:00";
                        String dtill = flEndStats.getText().toString()+ " 23:59:59";
                        List<Schedule> schedlist = dbhelper.getFilteredSchedules(-1,dfrom,dtill);
                        showFilteredStats(schedlist,inflater,lContainerStats);
                    });

                    flStartStats.setOnClickListener(v -> showDateDialog(v,flStartStats,Calendar.getInstance(),btnFlOk));
                    flEndStats.setOnClickListener(v -> showDateDialog(v,flEndStats,Calendar.getInstance(),btnFlOk));
                    flStartStats.setText(from.split(" ")[0]);
                    flEndStats.setText(till.split(" ")[0]);

                    lContainerStats.addView(filterRowStats);
                    String dfrom = flStartStats.getText().toString()+" 00:00:00";

                    String dtill = flEndStats.getText().toString()+" 23:59:59";

                    List<Schedule> scheds = dbhelper.getFilteredSchedules(-1,dfrom,dtill);
                    showFilteredStats(scheds,inflater,lContainerStats);
                    //textView.setText(getString(R.string.workDone, workdone[0],workdone[1]));
                    break;
                case 10:
                    bcounter=0;
                    if(NailTracker.edThisSched==null) {
                        rootView = getEditSchedView(rootView, container, inflater, null);
                    }else{
                        rootView = getEditSchedView(rootView, container, inflater, NailTracker.edThisSched);
                    }
                    break;
                default:
                    break;
            }
            dbhelper.close();
            return rootView;
        }

        private void makeCall() {
            int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
            } else {
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:"+phoneToCall));
                startActivity(i);
            }

        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 1:
                    if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                        if(phoneToCall.length()>10&&phoneToCall.length()<12)
                        makeCall();
                    } else {
                        System.out.println("Call Permission Not Granted");
                    }
                    break;

                default:
                    break;
            }
        }

        private void showFilteredStats(List<Schedule> schedlist, LayoutInflater inflater, LinearLayout lContainerStats){
            //System.out.println("showFilteredStats() called");
            //newt comment now doesn't matter
        //(0,'Покрытие',2000),(1,'Наращивание',2300),(2,'Снятие',800),(3,'Исправление',500),(4,'Всё включено',3000),(5,'Ресницы',4000)
            if(lContainerStats.getChildCount()>1)lContainerStats.removeViewsInLayout(1, lContainerStats.getChildCount() - 1);
            workTypes = dbhelper.getWorkTypes();
            //SparseIntArray wstats_done = new SparseIntArray(workTypes.size());
            HashMap<Integer,Integer> wstats_done = new HashMap<Integer,Integer>();
            HashMap<Integer,Integer> wstats_cancel = new HashMap<Integer,Integer>();
            HashMap<Integer,Integer> wstats_money = new HashMap<Integer,Integer>();
            for(WorkType wt : workTypes){
                //System.out.println(String.format("%s:%s",wt.getWid(),wt.getWtype()));
                wstats_cancel.put(wt.getWid(),0);
                wstats_done.put(wt.getWid(),0);
                wstats_money.put(wt.getWid(),0);
            }

            for (Schedule entry: schedlist){
                //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
                int ewt = entry.getWork_type();
                if(entry.getSstatus()==3){
                    wstats_cancel.put(ewt,wstats_cancel.get(ewt)+1);
                }
                if(entry.getSstatus()==1){
                    wstats_done.put(ewt,wstats_done.get(ewt)+1);
                    wstats_money.put(ewt,wstats_money.get(ewt)+entry.getCost());
                }
            }
            int[] total = {0,0,0};
            for(int i : wstats_done.values())total[0]+=i;
            for(int i : wstats_cancel.values())total[1]+=i;
            for(int i : wstats_money.values())total[2]+=i;
            for(WorkType wt : workTypes){
                View viewStats = inflater.inflate(R.layout.view_stats_layout, lContainerStats, false);
                TextView title = viewStats.findViewById(R.id.detail_title);
                TextView stats = viewStats.findViewById(R.id.stats_data);
                title.setText(wt.getWtype());
                stats.setText(getString(R.string.stats_line, wstats_done.get(wt.getWid()),wstats_cancel.get(wt.getWid()),wstats_money.get(wt.getWid())));
                lContainerStats.addView(viewStats);
            }
            View viewStats = inflater.inflate(R.layout.view_stats_layout, lContainerStats, false);
            TextView title = viewStats.findViewById(R.id.detail_title);
            TextView stats = viewStats.findViewById(R.id.stats_data);
            title.setText("Итого:");
            stats.setText(getString(R.string.stats_line, total[0],total[1],total[2]));
            lContainerStats.addView(viewStats);
        }

        private void showFiltereSchedules(List<Schedule> schedules, LayoutInflater inflater, ViewGroup lContainer) {
            //System.out.println("entries to display:" + schedules.size());
            //System.out.println("child count:"+lContainer.getChildCount());
            if(lContainer.getChildCount()>1) {
                lContainer.removeViewsInLayout(1, lContainer.getChildCount() - 1);
            }

            for (Schedule entry : schedules) {
                View erow = inflater.inflate(R.layout.view_list_item, lContainer,false);
                erow.setContentDescription(String.valueOf(entry.getGid()));
                TextView TVschedDate = erow.findViewById(R.id.entry_date);
                TextView tvWType = erow.findViewById(R.id.TVworkType);
                TextView tvName = erow.findViewById(R.id.entry_name);
                TextView tvComment = erow.findViewById(R.id.tvComment);
                TextView tvCost = erow.findViewById(R.id.entry_cost);
                TVschedDate.setText(entry.getStart_time());
                tvWType.setText(dbhelper.getWorkType(entry.getWork_type()));
                tvName.setText(entry.getClient());
                tvComment.setText(entry.getComment());
                tvCost.setText(String.valueOf(entry.getCost()));
                switch (entry.getSstatus()){
                    //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
                    default:
                        break;
                    case 1:
                        erow.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        break;
                    case 3:
                        erow.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        break;
                }
                lContainer.addView(erow);
            }
        }


        private void showTimeDialog(View cdev, TextView edTime) {
            bcounter=0;
            //System.out.println("contactDataET clicked");
            int hour = Integer.parseInt(edTime.getText().toString().split(":")[0]);//btime.get(Calendar.HOUR_OF_DAY);
            int minute = Integer.parseInt(edTime.getText().toString().split(":")[1]);//btime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(cdev.getContext(), (timePicker, selectedHour, selectedMinute) -> {
                //System.out.println("selected minute:"+selectedMinute);
                String shour = String.valueOf(selectedHour);
                if(String.valueOf(selectedHour).length()==1)shour="0"+shour;
                if(selectedMinute>9)
                    edTime.setText(String.format("%s:%s",shour,selectedMinute));
                else
                    edTime.setText(String.format("%s:0%s",shour,selectedMinute));

            }, hour, minute, true);
            mTimePicker.setTitle("Время записи");
            mTimePicker.show();
        }

        private void showDateDialog(View cdev, TextView edDate, Calendar bdate,Button action) {
            //System.out.println("contactDataET clicked");
            AlertDialog.Builder builder = new AlertDialog.Builder(cdev.getContext());
            builder.setTitle("Дата записи");
            DatePicker edDatePicker = new DatePicker(cdev.getContext());
            edDatePicker.setCalendarViewShown(false);
            edDatePicker.init(bdate.get(Calendar.YEAR), bdate.get(Calendar.MONTH),bdate.get(Calendar.DAY_OF_MONTH), (view, year, monthOfYear, dayOfMonth) -> {
                monthOfYear+=1;
                String month = (String.valueOf(monthOfYear).length()==1) ? "0"+String.valueOf(monthOfYear) : String.valueOf(monthOfYear);
                String day = (String.valueOf(dayOfMonth).length()==1) ? "0"+String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                edDate.setText(String.format("%s-%s-%s",year,month,day));
                if(action!=null){
                    action.callOnClick();
                }
            });
            builder.setView(edDatePicker);
            builder.setPositiveButton("OK", (dialog, which) -> System.out.println("OK"));
            builder.show();
        }

        @Override
        public void onResume(){
            super.onResume();
            workTypes = dbhelper.getWorkTypes();
        }

        private View getEditSchedView(View rootView, ViewGroup container, LayoutInflater inflater,Schedule sched) {

            rootView = inflater.inflate(R.layout.fragment_addsched, container, false);
            TextView edDate = rootView.findViewById(R.id.edDate);
            edDate.setOnClickListener(v1 -> {
                Calendar bdate = Calendar.getInstance();
                showDateDialog(v1,edDate,bdate,null);
            });
            TextView edTime = rootView.findViewById(R.id.edTime);
            edTime.setOnClickListener(v2 ->{
                showTimeDialog(v2,edTime);
            });

            Switch swIgnore = rootView.findViewById(R.id.swIgnore);
            //EditText edTimeM = rootView.findViewById(R.id.edTimeM);
            EditText edClient = rootView.findViewById(R.id.edClient);
            EditText edComment = rootView.findViewById(R.id.edComment);
            EditText edGid = rootView.findViewById(R.id.schedGid);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//            SimpleDateFormat stf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String rdt = sdf.format(new Date(System.currentTimeMillis() + 86400 * 1000));
            String rtt = NailTracker.stf.format(new Date(System.currentTimeMillis() + 86400 * 1000));
            Spinner spnWType = rootView.findViewById(R.id.spnWorkTypes);
            WorkType[] wtypes = new WorkType[workTypes.size()];
            for (int i = 0; i < workTypes.size(); i++) {
                wtypes[i] = workTypes.get(i);
            }
            SpinAdapter adapter = new SpinAdapter(context, android.R.layout.simple_spinner_item, wtypes);
            spnWType.setAdapter(adapter);
            if(sched==null) {
                swIgnore.setVisibility(View.INVISIBLE);
                edDate.setText(rdt);
                String edtimeVal = rtt.split(":")[0]+":00";
                edTime.setText(edtimeVal);
                //edTimeM.setText(rtt.split(":")[1]);
                //edTimeM.setText("00");
                edGid.setText("0");
            }else{
                swIgnore.setVisibility(View.VISIBLE);
                String[] tdata = sched.getStart_time().split(" ");
                edDate.setText(tdata[0]);
                String[] ttime = tdata[1].split(":");
                edTime.setText(String.format("%s:%s",ttime[0],ttime[1]));
//                edTime.setText(tdata[1].split(":")[0]);
//                edTimeM.setText(tdata[1].split(":")[1]);
                spnWType.setSelection(sched.getWork_type());
                edClient.setText(sched.getClient());
                edComment.setText(sched.getComment());
                edGid.setText(String.valueOf(sched.getGid()));
            }
            btnSaveSchedule = rootView.findViewById(R.id.btnSaveSchedule);
            NailTracker.fabHandler.fab.hide();//(View.INVISIBLE);
            btnSaveSchedule.setOnClickListener(this);
            return rootView;
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btnSaveSchedule) {
                boolean ignore =false;
                View rw = v.getRootView();
                TextView edDate = rw.findViewById(R.id.edDate);
                TextView edTime = rw.findViewById(R.id.edTime);
                Switch swIgnore = rw.findViewById(R.id.swIgnore);
                if(swIgnore.getVisibility()==View.VISIBLE){
                    ignore = swIgnore.isChecked();
                }
                //EditText edTimeM = rw.findViewById(R.id.edTimeM);
                EditText edGid = rw.findViewById(R.id.schedGid);
                StringBuilder schedTime = new StringBuilder().append(edDate.getText().toString());
                schedTime.append(" ").append(edTime.getText().toString()).append(":00");
                //System.out.println("shcedTime gonna be:"+schedTime);
                Spinner spnr = rw.findViewById(R.id.spnWorkTypes);
                WorkType wt = (WorkType) spnr.getSelectedItem();
                //System.out.println("Selected item:" + wt.getWtype() + "wid:" + wt.getWid());
                List<Schedule> schchk = dbhelper.CheckScheduleTime(schedTime.toString(),wt.getWid());
                EditText ecln = rw.findViewById(R.id.edClient);
                EditText ecmnt = rw.findViewById(R.id.edComment);
                Schedule newSched = new Schedule();
                newSched.setStart_time(schedTime.toString());
                newSched.setClient(ecln.getText().toString());
                if (ecmnt.getText().toString().compareTo("") != 0)
                    newSched.setComment(ecmnt.getText().toString());
                else {
                    newSched.setComment("");
                }
                newSched.setWork_type(wt.getWid());
                newSched.setCost(0);
                newSched.setSstatus(0);
                if (newSched.getClient().compareTo("") == 0) {
                    ecln.setBackgroundColor(Color.RED);
                    return;
                }

                Schedule found = null;
                if(!ignore)
                for(Schedule sc: schchk){
                    if(sc.getGid()!=Integer.parseInt(edGid.getText().toString())){
                        found = sc;
                    }
                }
                if(edGid.getText().toString().compareTo("0")!=0&&found==null){
                    //update gid entry
                    newSched.setGid(Integer.parseInt(edGid.getText().toString()));
                    dbhelper.InsertSchedule(newSched,true);
                    NailTracker.edThisSched = null;
                    dbhelper.close();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, PlaceholderFragment.newInstance(1))
                            .commit();
                }else {
                    if (found == null) {
                        //System.out.println("We can schedule this");
                        //EditText ecst = rw.findViewById(R.id.edCost);
                        NailTracker.edThisSched = null;
                        dbhelper.InsertSchedule(newSched,false);
                        dbhelper.close();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, PlaceholderFragment.newInstance(1))
                                .commit();
                    } else {
                        StringBuilder ttext = new StringBuilder();
                        //Schedule sched = schchk.get(0);
                        ttext.append("На это время уже есть запись:\n").append(found.getStart_time());
                        ttext.append("\n").append(found.getClient());
                        ttext.append(" - ").append(dbhelper.getWorkType(found.getWork_type()));
                        Toast.makeText(getContext(), ttext.toString(), Toast.LENGTH_LONG).show();
                        //System.out.println("Someone already scheduled on this tim");
                    }
                }

            }
        }
    }

    static class SpinAdapter extends ArrayAdapter<WorkType> {
        private Context context;
        private WorkType[] values;

        SpinAdapter(Context context, int tvResId, WorkType[] values) {
            super(context, tvResId, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values.length;
        }

        @Override
        public WorkType getItem(int position) {
            //System.out.println("selected index:" + position + "|" + values[position]);
            return values[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView label = (TextView)View.inflate(context,R.layout.wtype_spinner_item,null);//new TextView(context);
            label.setTextColor(Color.BLACK);
//            label.setTextSize(25.0f);
            // Then you can get the current item using the values array (Users array) and the current position
            // You can NOW reference each method you has created in your bean object (User class)
            label.setText(values[position].getWtype());
            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView label = (TextView)View.inflate(context,R.layout.wtype_spinner_item,null);//new TextView(context);
            label.setTextColor(Color.BLACK);
//            label.setTextSize(25.0f);
            label.setText(values[position].getWtype());
            return label;
        }

    }

}
