package com.sanlair.nailtracker;

class Schedule {
    private int gid;
    private String start_time;
    private String end_time;
    private String client;
    private int cost;
    private String comment;
    private int sstatus; //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
    private int work_type;

    Schedule(){}
    Schedule(int gid,String st,String et,String cl, int cost, String cmnt,int ss,int wt){
        this.gid = gid;
        this.start_time=st;
        this.end_time=et;
        this.client=cl;
        this.cost=cost;
        this.comment=cmnt;
        this.sstatus=ss;
        this.work_type=wt;

    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getSstatus() {
        return sstatus;
    }

    public void setSstatus(int sstatus) {
        this.sstatus = sstatus;
    }

    public int getWork_type() {
        return work_type;
    }

    public void setWork_type(int work_type) {
        this.work_type = work_type;
    }
}
