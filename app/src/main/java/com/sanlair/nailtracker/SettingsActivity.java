package com.sanlair.nailtracker;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public class SettingsActivity extends AppCompatActivity {
    public StorageHelper dbhelper = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final EditText etLength = findViewById(R.id.etElength);
        final EditText etElengthLong = findViewById(R.id.etElengthLong);

        Button btnPrefSave = findViewById(R.id.btnPrefSave);
        Button btnBackup = findViewById(R.id.btnDBBackup);
        Button btnRestore = findViewById(R.id.btnDBRestore);
        dbhelper = new StorageHelper(this);
        float curval = Float.parseFloat(dbhelper.getUdataByName("elength"))/60/60;
        float curvalL =Float.parseFloat(dbhelper.getUdataByName("ellength"))/60/60;
        etLength.setText(String.valueOf(curval));
        etElengthLong.setText(String.valueOf(curvalL));
        btnPrefSave.setOnClickListener(v -> {
            String etl = etLength.getText().toString();
            String etll = etElengthLong.getText().toString();
            int secs = (int)(Float.parseFloat(etl)*60*60);
            int secsL = (int)(Float.parseFloat(etll)*60*60);
            //System.out.println("newval:"+secs);
            dbhelper.UpdateUdata("elength",String.valueOf(secs));
            dbhelper.UpdateUdata("ellength",String.valueOf(secsL));
            dbhelper.close();
            dbhelper=null;
            finish();
        });

        if(!isReadStoragePermissionGranted()){
            Toast.makeText(getApplicationContext(),"Восстановлений из резервных копий не возможно, т.к отсутствует разрешение на чтение",Toast.LENGTH_LONG).show();
        }
        if(!isWriteStoragePermissionGranted()){
            Toast.makeText(getApplicationContext(),"Создание резервных копий не возможно, т.к отсутствует разрешение на запись",Toast.LENGTH_LONG).show();
        }


        btnBackup.setOnClickListener(v -> {
            try {
                File sd = Environment.getExternalStorageDirectory();
                if (sd.canWrite()) {
                    //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    String backupDBPath =  String.format("%s.bak", "sanlair.nailtrack.db");
                    File currentDB = getApplicationContext().getDatabasePath(dbhelper.getWritableDatabase().getPath());
                    File backupDB = new File(sd, backupDBPath);
                    if(dbhelper.getWritableDatabase().isOpen())dbhelper.close();
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(getApplicationContext(),"backup completed",Toast.LENGTH_LONG).show();
                }else {
                    System.out.println("SD Card unable to write");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnRestore.setOnClickListener(v -> {
            try {
                File sd = Environment.getExternalStorageDirectory();
                if (sd.canWrite()) {
                    File backupDB = getApplicationContext().getDatabasePath(dbhelper.getWritableDatabase().getPath());
                    String backupDBPath = String.format("%s.bak","sanlair.nailtrack.db");
                    File currentDB = new File(sd, backupDBPath);
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(getApplicationContext(),"Import Successful",Toast.LENGTH_LONG).show();
                    System.out.println("Import Successful");
                }else{
                    Toast.makeText(getApplicationContext(),"Unable to write SDcard",Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    public boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else {
            return true;
        }
    }

    public boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else {
            return true;
        }
    }


}
