package com.sanlair.nailtracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class StorageHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME="sanlair.nailtracker.db";
    private static final int SCHEMA_VERSION=1;
    StorageHelper(Context context)
    {
        super(context,DATABASE_NAME,null,SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE udata(pname TEXT UNIQUE PRIMARY KEY, pval TEXT);");
        db.execSQL("CREATE TABLE wtypes(wid INTEGER PRIMARY KEY, wtype TEXT,len INTEGER DEFAULT(0))");
        db.execSQL("CREATE TABLE schedules(gid INTEGER PRIMARY KEY, start_time DEFAULT (datetime('now','localtime')),end_time DEFAULT(NULL),client TEXT,cost INTEGER DEFAULT(0),comment TEXT,sstatus INTEGER DEFAULT(0),wtype INTEGER);");
        //db.execSQL("CREATE TABLE clients(id INTEGER PRIMARY KEY, name TEXT, phone TEXT, uname TEXT);");
        db.execSQL("INSERT INTO wtypes(wid,wtype,len) VALUES(0,'Покрытие',0),(1,'Наращивание',1),(2,'Снятие',2),(3,'Исправление',2),(4,'Всё включено',0),(5,'Ресницы',1);");
        db.execSQL("INSERT INTO udata(pname,pval) VALUES('elength','7200')");
        db.execSQL("INSERT INTO udata(pname,pval) VALUES('ellength','10800')");
        db.execSQL("INSERT INTO udata(pname,pval) VALUES('eslength','1800')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int OldVersion, int NewVersion){ }

    public void TruncateUdata(){
        String truncate ="DELETE FROM udata;";
        getWritableDatabase().execSQL(truncate);
    }

    public void execSql(String sql){
        getWritableDatabase().execSQL(sql);
    }

    String getUdataByName(String pname){
        String query="SELECT pval FROM udata WHERE pname='"+pname+"'";
        SQLiteStatement result = getReadableDatabase().compileStatement(query);
        return (result.simpleQueryForString());
    }

    public void DeleteUdata(String pname){
        String delete ="DELETE from udata WHERE pname="+pname+";";
        getWritableDatabase().execSQL(delete);
    }

    public void InsertUdata(String pname,String pval){
        ContentValues cv = new ContentValues();
        cv.put("pname",pname);
        cv.put("pval",pval);
        getWritableDatabase().beginTransaction();
        getWritableDatabase().insert("udata",null,cv);
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public void UpdateUdata(String pname,String pval){
        ContentValues cv = new ContentValues();
        cv.put("pname",pname);
        cv.put("pval",pval);
        getWritableDatabase().beginTransaction();
        getWritableDatabase().update("udata",cv,"pname="+"'"+pname+"'",null);
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public String getWorkType(int id){
        String result ="";
        String query = "SELECT wtype FROM wtypes WHERE wid="+String.valueOf(id)+";";
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        try{
            while (cursor.moveToNext()){
                result = cursor.getString(cursor.getColumnIndex("wtype"));
            }
        }
        finally {
            cursor.close();
        }
        return result;
    }
    public List<WorkType> getWorkTypes(){
        List<WorkType> result = new ArrayList<WorkType>();
        String query = "SELECT wid,wtype,len FROM wtypes order by wid;";
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        try{
            while (cursor.moveToNext()){
                result.add(new WorkType(cursor.getInt(cursor.getColumnIndex("wid")),
                        cursor.getString(cursor.getColumnIndex("wtype")),
                        cursor.getInt(cursor.getColumnIndex("len"))));
            }
        }finally {
            cursor.close();
        }
        return result;
    }

    public List<Schedule> getNearestSchedules(){
        //sstatus
        List<Schedule> result = new ArrayList<Schedule>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String rdt = sdf.format(new Date(System.currentTimeMillis() - 86400 * 1000));
        String query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE start_time>='"+rdt+"';";
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        try{
            while(cursor.moveToNext()){
                result.add(new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")))
                        );
            }
        }
        finally {
            cursor.close();
        }
        return result;
    }

    public List<Schedule> CheckScheduleTime(String schedTime, int wid){
        //sstatus
        //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
        //time
        //6300 = 1:45
        //5400 = 1:30
        List<Schedule> result = new ArrayList<>();
        String from,lfrom,ltill,till,sfrom,still;
        from = lfrom = ltill = till = sfrom = still = "";
        int secs = Integer.parseInt(getUdataByName("elength"));
        int secsL = Integer.parseInt(getUdataByName("ellength"));
        int secsS = Integer.parseInt(getUdataByName("eslength"));
        //System.out.println("checking time:"+schedTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {

            Date date = sdf.parse(schedTime);
            date.setTime(date.getTime()-(secs-60)*1000);
            from = sdf.format(date);

            date = sdf.parse(schedTime);
            date.setTime(date.getTime()+(secs-60)*1000);
            till = sdf.format(date);

            date = sdf.parse(schedTime);
            date.setTime(date.getTime()-(secsL-60)*1000);
            lfrom = sdf.format(date);

            date = sdf.parse(schedTime);
            date.setTime(date.getTime()+(secsL-60)*1000);
            ltill = sdf.format(date);

            date = sdf.parse(schedTime);
            date.setTime(date.getTime()-(secsS-60)*1000); //+
            sfrom = sdf.format(date);

            date = sdf.parse(schedTime);
            date.setTime(date.getTime()+(secsS-60)*1000);
            still = sdf.format(date);

            if(wid==1||wid==5){
                //System.out.println(String.format("wid=%s setting up to longtill",wid));
                till = ltill;
            }
            if(wid==2||wid==3){
                //System.out.println(String.format("wid=%s setting up to short ",wid));
                //System.out.println("wid:"+wid);
                till = still;
            }
            //System.out.println("parsed_date:"+date);
            //System.out.println("gonna look entries from:"+from+" till:"+till);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE start_time>='"+from+"' and start_time<='"+till+"' and wtype not in(1,2,3,5) and sstatus in(0,2);";
        try (Cursor cursor = getReadableDatabase().rawQuery(query, null)){
            //System.out.println("checkig normal entries with query:"+query);
            while (cursor.moveToNext()) {
                result.add(new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")))
                );
                //System.out.println(String.format("we have a normal result on:%s till %s",result.get(result.size()-1).getStart_time(),till));
            }
        }
        //this is totally wrong but it's temporarly

        query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE start_time>='"+lfrom+"' and start_time<='"+till+"' and wtype in(1,5) and sstatus in(0,2);";
        try (Cursor cursor = getReadableDatabase().rawQuery(query, null)){
            //System.out.println("checkig long entries with query:"+query);
            while (cursor.moveToNext()) {
                result.add(new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")))
                );
                //System.out.println(String.format("we have a long result on:%s till %s",result.get(result.size()-1).getStart_time(),till));
            }
        }

        query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE start_time>='"+sfrom+"' and start_time<='"+till+"' and wtype in(2,3) and sstatus in(0,2);";
        try (Cursor cursor = getReadableDatabase().rawQuery(query, null)){
            //System.out.println("checkig short entries with query:"+query);
            while (cursor.moveToNext()) {
                result.add(new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")))
                );
                //System.out.println(String.format("we have a short result on:%s till %s",result.get(result.size()-1).getStart_time(),till));
            }
        }

        return result;
    }

    public List<Schedule> getFilteredSchedules(int filter,String dateFrom,String dateTill){
        //sstatus
        //0 = scheduled, 3=cancelled, 2=rescheduled, 1=finished
        List<Schedule> result = new ArrayList<Schedule>();
        String fl ="";
        String sort = "asc";
        switch (filter){
            case 0:
                fl="sstatus in(0,2)";
                break;
            case 1:
                fl="sstatus=1";
                break;
            case 2:
                fl="sstatus=2";
                break;
            case 3:
                fl="sstatus=3";
                break;
            case 12:
                fl="sstatus!=3";
                break;
            case -1:
                if(dateFrom!=null && dateTill!=null) {
                    fl = String.format("start_time>='%s' and start_time<='%s'", dateFrom, dateTill);
                    sort="desc";
                }
                break;
            default:
                fl="sstatus in(0,2)";
                break;
        }
        String query = String.format("SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE %s order by start_time %s;",fl,sort);
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        try{
            while(cursor.moveToNext()){
                result.add(new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")))
                );
            }
        }
        finally {
            cursor.close();
        }
        return result;
    }

    void UpdateSchedule(Schedule sched){
        ContentValues cv = new ContentValues();
        //cv.put("gid",sched.getGid());
        cv.put("start_time",sched.getStart_time());
        cv.put("end_time",sched.getEnd_time());
        cv.put("client",sched.getClient());
        cv.put("cost",sched.getCost());
        cv.put("comment",sched.getComment());
        cv.put("sstatus",sched.getSstatus());
        cv.put("wtype",sched.getWork_type());
        getWritableDatabase().beginTransaction();
        getWritableDatabase().update("schedules",cv,"gid="+"'"+sched.getGid()+"'",null);
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    void changeStatus(String gid,int status){
        ContentValues cv = new ContentValues();
        cv.put("sstatus",status);
        getWritableDatabase().beginTransaction();
        getWritableDatabase().update("schedules",cv,"gid="+"'"+gid+"'",null);
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    void changeStatus(String gid,int status,int cost){
        ContentValues cv = new ContentValues();
        cv.put("sstatus",status);
        cv.put("cost",cost);
        getWritableDatabase().beginTransaction();
        getWritableDatabase().update("schedules",cv,"gid="+"'"+gid+"'",null);
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public void InsertSchedule(Schedule sched,boolean update){
        ContentValues cv = new ContentValues();
        cv.put("start_time",sched.getStart_time());
        cv.put("end_time",sched.getEnd_time());
        cv.put("client",sched.getClient());
        cv.put("cost",sched.getCost());
        cv.put("comment",sched.getComment());
        cv.put("sstatus",sched.getSstatus());
        cv.put("wtype",sched.getWork_type());
        getWritableDatabase().beginTransaction();
        if(update){
            getWritableDatabase().update("schedules",cv,String.format("gid=%s",sched.getGid()),null);
        }else{
            getWritableDatabase().insert("schedules",null,cv);
        }
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }


    int[] getFinishedSchedules(){
        int[] result = {0,0}; //entries, sum
        String query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE sstatus=1 order by start_time asc;";
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        try{
            while(cursor.moveToNext()){
                result[0]+=1;
                result[1]+=cursor.getInt(cursor.getColumnIndex("cost"));
            }
        }
        finally {
            cursor.close();
        }
        return result;
    }

    Schedule getSchedule(int gid){
        Schedule result = new Schedule();
        String query = "SELECT gid,start_time,end_time,client,cost,comment,sstatus,wtype FROM schedules WHERE gid=? limit 1;";
        Cursor cursor = getReadableDatabase().rawQuery(query, new String[] {String.valueOf(gid)});
        try{
            while (cursor.moveToNext()){
               result = new Schedule(cursor.getInt(cursor.getColumnIndex("gid")),
                        cursor.getString(cursor.getColumnIndex("start_time")),
                        cursor.getString(cursor.getColumnIndex("end_time")),
                        cursor.getString(cursor.getColumnIndex("client")),
                        cursor.getInt(cursor.getColumnIndex("cost")),
                        cursor.getString(cursor.getColumnIndex("comment")),
                        cursor.getInt(cursor.getColumnIndex("sstatus")),
                        cursor.getInt(cursor.getColumnIndex("wtype")));
            }
        }finally {
            cursor.close();
        }
        return result;
    }

}