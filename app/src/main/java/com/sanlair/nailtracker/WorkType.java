package com.sanlair.nailtracker;

class WorkType {
    private int wid;
    private String wtype;
    private int len;

    WorkType(int w, String wt, int c){
        this.wid=w;
        this.wtype=wt;
        this.len=c;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public String getWtype() {
        return wtype;
    }

    public void setWtype(String wtype) {
        this.wtype = wtype;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

}
