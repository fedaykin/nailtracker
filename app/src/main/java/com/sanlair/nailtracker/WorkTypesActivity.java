package com.sanlair.nailtracker;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class WorkTypesActivity extends AppCompatActivity implements View.OnClickListener {
    List<WorkType> wtypes = null;
    StorageHelper dbhelper = null;
    LinearLayout container = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_types);
        dbhelper = new StorageHelper(this);
        container = findViewById(R.id.container);
        generateList();
    }

    protected void generateList(){
        wtypes = dbhelper.getWorkTypes();
        Button btnAdd = container.findViewById(R.id.Add);
        container.removeAllViews();
        btnAdd.setOnClickListener(this);
        container.addView(btnAdd);
        for (WorkType wt: wtypes) {
            View row = getLayoutInflater().inflate(R.layout.work_types_item, container, false);
            TextView wtypename = row.findViewById(R.id.wtypename);
            wtypename.setText(wt.getWtype());
            Button btnWtRemove = row.findViewById(R.id.btnWtRemove);
            btnWtRemove.setContentDescription(String.format("delete-%s",wt.getWid()));
            btnWtRemove.setOnClickListener(this);
            container.addView(row);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getContentDescription().toString().contains("delete")){
            int id = Integer.parseInt(v.getContentDescription().toString().split("-")[1]);
            dbhelper.execSql(String.format("DELETE FROM wtypes WHERE wid='%s'",id));
            dbhelper.execSql(String.format("DELETE FROM schedules WHERE wtype='%s'",id));
            generateList();
        }
        if(v.getId()==R.id.Add){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Новый тип работы");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dbhelper.execSql(String.format("INSERT INTO wtypes(wtype) VALUES('%s')",input.getText().toString()));
                    generateList();
                }
            });
            builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }


}
